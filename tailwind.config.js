/** @type {import('tailwindcss').Config} */

const colors = require('tailwindcss/colors')

module.exports = {
    content: ['templates/**/*.html.twig', 'assets/js/**/*.js'],
    theme: {
        colors: {
            main: {
                50: '#F8F5FF',
                100: '#EEE6FE',
                200: '#D9C8FD',
                300: '#C1A6FC',
                400: '#9F74FB',
                500: '#570DF8',
                600: '#5007EE',
                700: '#4B06DF',
                800: '#4106C1',
                900: '#3A05AE',
            },
        },
    },
    plugins: [
        require('@tailwindcss/forms'),
        require('daisyui'),
        require('@tailwindcss/typography'),
    ],
    daisyui: {
        themes: [
            {
                aih: {
                    ...require('daisyui/src/colors/themes')['[data-theme=light]'],
                    primary: '#009baa',
                    secondary: '#a71e26',
                    accent: '#a71e26',
                    'accent-focus': '#eeeeee',
                },

                test: {
                    primary: colors.sky[500],
                    'primary-focus': colors.sky[600],
                    'primary-content': colors.sky[100],

                    secondary: colors.blue[400],
                    'secondary-focus': colors.blue[500],
                    'secondary-content': colors.blue[50],

                    accent: colors.rose[600],
                    'accent-focus': colors.rose[700],

                    'base-100': colors.neutral[50],
                    'base-200': colors.neutral[100],
                    'base-300': colors.neutral[200],

                    '--rounded-box': '0.45rem', // border radius rounded-box utility class, used in card and other large boxes
                    '--rounded-btn': '0.325rem', // border radius rounded-btn utility class, used in buttons and similar element
                    '--rounded-badge': '1.9rem', // border radius rounded-badge utility class, used in badges and similar
                    '--animation-btn': '0.25s', // duration of animation when you click on button
                    '--animation-input': '0.2s', // duration of animation for inputs like checkbox, toggle, radio, etc
                    '--btn-text-case': 'uppercase', // set default text transform for buttons
                    '--btn-focus-scale': '0.95', // scale transform of button when you focus on it
                    '--border-btn': '1px', // border width of buttons
                    '--tab-border': '1px', // border width of tabs
                    '--tab-radius': '0.5rem', // border radius of tabs
                },
            },
            'light',
            'dark',
            'cupcake',
            'bumblebee',
            'emerald',
            'corporate',
            'synthwave',
            'retro',
            'cyberpunk',
            'valentine',
            'halloween',
            'garden',
            'forest',
            'aqua',
            'lofi',
            'pastel',
            'fantasy',
            'wireframe',
            'black',
            'luxury',
            'dracula',
            'cmyk',
            'autumn',
            'business',
            'acid',
            'lemonade',
            'night',
            'coffee',
            'winter',
        ],
    },
}
